let https = require("https");
let fs = require("fs");
let dotenv = require("dotenv");

dotenv.config();

if (!process.env.SCREEPS_BRANCH || !process.env.SCREEPS_TOKEN) {
  console.log("Please create a .env file with the following contents:");
  console.log("SCREEPS_BRANCH=my-branch");
  console.log("SCREEPS_TOKEN=my-api-token");
  process.exit(1);
}

let modules = {};

fs.readdirSync("./dist").forEach((filename) => {
  let name = filename.replace(".js", "");
  let content = fs.readFileSync("./dist/" + filename).toString("utf8");
  modules[name] = content;
});

let data = {
  branch: process.env.SCREEPS_BRANCH,
  modules: modules,
};

let req = https.request({
  hostname: "screeps.com",
  port: 443,
  path: "/api/user/code",
  method: "POST",
  headers: {
    "Content-Type": "application/json; charset=utf-8",
    "X-Token": process.env.SCREEPS_TOKEN,
  },
});

req.write(JSON.stringify(data));
req.end();

console.log("Deployed code to branch " + process.env.SCREEPS_BRANCH);
