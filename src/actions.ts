import { ActionResult } from "./types";

function performAction(creep: Creep): ActionResult {
  let action = creep.memory.action;

  if (!action) {
    return "failure";
  }

  switch (action.type) {
    case "harvest": {
      let container = Game.getObjectById(action.containerId!);

      if (container) {
        if (!creep.pos.isEqualTo(container)) {
          creep.moveTo(container, {
            visualizePathStyle: { stroke: "#ff8833" },
          });
          return "in-progress";
        }

        if (
          container.hits < container.hitsMax &&
          creep.store.getUsedCapacity(RESOURCE_ENERGY) > 0
        ) {
          creep.repair(container);
          return "in-progress";
        }
      }

      let source = Game.getObjectById(action.sourceId)!;
      switch (creep.harvest(source)) {
        case OK:
          if (
            creep.store.getFreeCapacity(RESOURCE_ENERGY) > 0 &&
            !action.forever
          )
            return "success";
          else {
            return "in-progress";
          }
        case ERR_NOT_IN_RANGE:
          creep.moveTo(source, {
            visualizePathStyle: { stroke: "#ffff33" },
          });
          return "in-progress";
        default:
          return "failure";
      }
    }

    case "pickup": {
      let resource = Game.getObjectById(action.resourceId)!;
      switch (creep.pickup(resource)) {
        case OK:
          return "success";
        case ERR_NOT_IN_RANGE:
          creep.moveTo(resource, {
            visualizePathStyle: { stroke: "#ffff33" },
          });
          return "in-progress";
        default:
          return "failure";
      }
    }

    case "withdraw": {
      let target = Game.getObjectById(action.targetId)!;
      switch (creep.withdraw(target, RESOURCE_ENERGY)) {
        case OK:
          return "success";
        case ERR_NOT_IN_RANGE:
          creep.moveTo(target, {
            visualizePathStyle: { stroke: "#ffff33" },
          });
          return "in-progress";
        default:
          return "failure";
      }
    }

    case "transfer": {
      let target = Game.getObjectById(action.targetId)!;
      switch (creep.transfer(target, RESOURCE_ENERGY)) {
        case OK:
          return "success";
        case ERR_NOT_IN_RANGE:
          creep.moveTo(target, {
            visualizePathStyle: { stroke: "#3366ff" },
          });
          return "in-progress";
        default:
          return "failure";
      }
    }

    case "build": {
      let target = Game.getObjectById(action.targetId)!;
      switch (creep.build(target)) {
        case OK:
          if (creep.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
            return "in-progress";
          } else {
            return "success";
          }
        case ERR_NOT_IN_RANGE:
          creep.moveTo(target, {
            visualizePathStyle: { stroke: "#66ff66" },
          });
          return "in-progress";
        default:
          return "failure";
      }
    }

    case "upgrade": {
      let controller = Game.getObjectById(action.controllerId)!;
      switch (creep.upgradeController(controller)) {
        case OK:
          if (creep.store.getUsedCapacity(RESOURCE_ENERGY) > 0) {
            return "in-progress";
          } else {
            return "success";
          }
        case ERR_NOT_IN_RANGE:
          creep.moveTo(controller, {
            visualizePathStyle: { stroke: "#ff66ff" },
          });
          return "in-progress";
        default:
          return "failure";
      }
    }
  }
}

export = { performAction };
