function run(tower: StructureTower) {
  // Attack hostiles
  let hostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
  if (hostile) {
    tower.attack(hostile);
    return;
  }

  if (
    tower.store.getUsedCapacity(RESOURCE_ENERGY) <
    0.5 * tower.store.getCapacity(RESOURCE_ENERGY)
  ) {
    return;
  }

  // Repair roads
  let roads = tower.room.find(FIND_STRUCTURES, {
    filter: (structure) =>
      structure.structureType == STRUCTURE_ROAD &&
      structure.hits < structure.hitsMax,
  });
  let prioritizedRoads = _.sortBy(roads, (structure) => structure.hits);
  let road = _.find(prioritizedRoads);
  if (road) {
    tower.repair(road);
    return;
  }

  // Repair walls
  let walls = tower.room.find(FIND_STRUCTURES, {
    filter: (structure) =>
      (structure.structureType == STRUCTURE_RAMPART ||
        structure.structureType == STRUCTURE_WALL) &&
      structure.hits < structure.hitsMax,
  });
  let prioritizedWalls = _.sortBy(walls, (structure) => structure.hits);
  let wall = _.find(prioritizedWalls);
  if (wall && wall.hits < 300_000) {
    tower.repair(wall);
    return;
  }

  let storageStructures = tower.room.find(FIND_STRUCTURES, {
    filter: (structure) =>
      structure.structureType == STRUCTURE_STORAGE ||
      structure.structureType == STRUCTURE_CONTAINER,
  });
  let storedEnergy = _.sum(storageStructures, (structure) =>
    structure.store.getUsedCapacity(RESOURCE_ENERGY),
  );

  if (wall && wall.hits < 3_000_000 && storedEnergy > 300_000) {
    tower.repair(wall);
    return;
  }
}

export = { run };
