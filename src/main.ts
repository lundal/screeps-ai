import { Action, Role, RoleModule } from "./types";
import harvester = require("./role.harvester");
import staticHarvester = require("./role.static-harvester");
import distributor = require("./role.distributor");
import collector = require("./role.collector");
import builder = require("./role.builder");
import upgrader = require("./role.upgrader");
import tower = require("./tower");
import spawn = require("./spawn");

declare global {
  namespace _ {}
  interface CreepMemory {
    role?: Role;
    action?: Action;
  }
}

let roles: Record<Role, RoleModule> = {
  harvester: harvester,
  "static-harvester": staticHarvester,
  distributor: distributor,
  collector: collector,
  builder: builder,
  upgrader: upgrader,
};

function loop() {
  for (let name in Memory.creeps) {
    if (!Game.creeps[name]) {
      delete Memory.creeps[name];
      console.log("Deleting non-existing creep memory: ", name);
    }
  }

  if (Game.cpu.bucket >= PIXEL_CPU_COST) {
      Game.cpu.generatePixel();
  }

  _.forEach(Game.spawns, (_spawn) => {
    spawn.run(_spawn, roles);
  });

  _.forEach(Game.creeps, (creep) => {
    if (creep.memory.role) {
      roles[creep.memory.role].run(creep);
    }
  });

  _.forEach(Game.structures, (structure) => {
    if (structure.structureType == STRUCTURE_TOWER) {
      tower.run(structure as StructureTower);
    }
  });
}

export = { loop };
