import actions = require("./actions");
import bodies = require("./bodies");

function getBodyParts(energyLimit: number): BodyPartConstant[] | undefined {
  // No point in harvesting faster than energy replenishes
  let partLimit =
    2 + SOURCE_ENERGY_CAPACITY / ENERGY_REGEN_TIME / HARVEST_POWER;
  return bodies.build([MOVE, CARRY, WORK], [WORK], energyLimit, partLimit);
}

function run(creep: Creep) {
  if (!creep.memory.action) {
    let source = creep.pos.findClosestByPath(FIND_SOURCES, {
      filter: (source) =>
        // Only one static harvester per source
        !Object.values(Game.creeps).some((otherCreep) => {
          let action = otherCreep.memory.action;
          return (
            otherCreep.memory.role == "static-harvester" &&
            action &&
            action.type == "harvest" &&
            action.sourceId == source.id
          );
        }),
      maxRooms: 1,
    });
    if (source) {
      let container = source.pos
        .findInRange(FIND_STRUCTURES, 1)
        .find((structure) => structure.structureType == STRUCTURE_CONTAINER);

      creep.memory.action = {
        type: "harvest",
        sourceId: source.id,
        containerId: container?.id,
        forever: true,
      };
    }
  }

  switch (actions.performAction(creep)) {
    case "success":
    case "failure":
      delete creep.memory.action;
  }
}

export = { getBodyParts, run };
