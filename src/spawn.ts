import { Role, RoleModule } from "./types";

function run(spawn: StructureSpawn, roles: Record<Role, RoleModule>) {
  if (spawn.spawning) {
    var creep = Game.creeps[spawn.spawning.name];
    spawn.room.visual.text(
      "🛠️" + creep.memory.role,
      spawn.pos.x + 1,
      spawn.pos.y,
      { align: "left", opacity: 0.5 },
    );
    return;
  }
  if (spawn.room.energyAvailable < SPAWN_ENERGY_CAPACITY) {
    return;
  }
  let role = determineMostNeededRole(spawn);
  if (!role) {
    return;
  }
  let bodyParts = roles[role].getBodyParts(spawn.room.energyAvailable);
  if (!bodyParts) {
    return;
  }
  console.log("Spawning " + role);
  spawn.spawnCreep(bodyParts, role + "-" + Game.time, { memory: { role } });
}

function determineMostNeededRole(spawn: StructureSpawn): Role | undefined {
  // TODO: Count spawning creeps
  // TODO: Discount end of life creeps
  let harvesters = spawn.room.find(FIND_MY_CREEPS, {
    filter: (creep) => creep.memory.role == "harvester",
  });
  let staticHarvesters = spawn.room.find(FIND_MY_CREEPS, {
    filter: (creep) => creep.memory.role == "static-harvester",
  });
  let distributors = spawn.room.find(FIND_MY_CREEPS, {
    filter: (creep) => creep.memory.role == "distributor",
  });
  let collectors = spawn.room.find(FIND_MY_CREEPS, {
    filter: (creep) => creep.memory.role == "collector",
  });
  let upgraders = spawn.room.find(FIND_MY_CREEPS, {
    filter: (creep) => creep.memory.role == "upgrader",
  });
  let builders = spawn.room.find(FIND_MY_CREEPS, {
    filter: (creep) => creep.memory.role == "builder",
  });
  let storages = spawn.room.find(FIND_STRUCTURES, {
    filter: (structure) => structure.structureType == STRUCTURE_STORAGE,
  });
  let containers = spawn.room.find(FIND_STRUCTURES, {
    filter: (structure) => structure.structureType == STRUCTURE_CONTAINER,
  });
  let storedEnergy = _.sum([...storages, ...containers], (structure) =>
    structure.store.getUsedCapacity(RESOURCE_ENERGY),
  );

  if (
    harvesters.length == 0 &&
    staticHarvesters.length == 0 &&
    storedEnergy < spawn.room.energyCapacityAvailable
  ) {
    return "harvester";
  }

  if (distributors.length < 1) {
    return "distributor";
  }

  if (staticHarvesters.length < spawn.room.find(FIND_SOURCES).length) {
    return "static-harvester";
  }

  if (collectors.length < 1 && storages.length > 0) {
    return "collector";
  }

  if (upgraders.length < 1) {
    return "upgrader";
  }

  if (spawn.room.find(FIND_CONSTRUCTION_SITES).length > 0) {
    if (builders.length < 1 || (builders.length < 2 && storedEnergy > 10_000)) {
      return "builder";
    }
  }

  return undefined;
}

export = { run };
