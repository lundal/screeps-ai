function energyCost(bodyParts: BodyPartConstant[]): number {
  return _.sum(bodyParts, (bodyPart) => BODYPART_COST[bodyPart]);
}

function spawnTime(bodyParts: BodyPartConstant[]): number {
  return _.sum(bodyParts, (_bodyPart) => CREEP_SPAWN_TIME);
}

function build(
  baseParts: BodyPartConstant[],
  extensionParts: BodyPartConstant[],
  energyLimit: number,
  partLimit: number,
): BodyPartConstant[] | undefined {
  let energyLeft = energyLimit - energyCost(baseParts);
  let bodyParts: BodyPartConstant[] = baseParts;

  if (energyLeft < 0) {
    return undefined;
  }

  while (
    energyLeft >= energyCost(extensionParts) &&
    partLimit >= bodyParts.length + extensionParts.length
  ) {
    energyLeft -= energyCost(extensionParts);
    bodyParts.push(...extensionParts);
  }

  return bodyParts;
}

export = { energyCost, spawnTime, build };
