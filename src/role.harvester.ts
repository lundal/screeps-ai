import actions = require("./actions");
import bodies = require("./bodies");

function getBodyParts(energyLimit: number): BodyPartConstant[] | undefined {
  return bodies.build(
    [MOVE, CARRY, WORK],
    [MOVE, CARRY, WORK],
    energyLimit,
    15,
  );
}

function run(creep: Creep) {
  if (!creep.memory.action) {
    if (creep.store.getUsedCapacity(RESOURCE_ENERGY) == 0) {
      let source = creep.pos.findClosestByPath(FIND_SOURCES, {
        maxRooms: 1,
      });
      if (source) {
        creep.memory.action = {
          type: "harvest",
          sourceId: source.id,
          forever: false,
        };
      }
    } else {
      let structure = creep.pos.findClosestByPath(FIND_MY_STRUCTURES, {
        filter: (structure) =>
          (structure.structureType == STRUCTURE_EXTENSION ||
            structure.structureType == STRUCTURE_SPAWN) &&
          structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0,
        maxRooms: 1,
      });
      if (structure) {
        creep.memory.action = { type: "transfer", targetId: structure.id };
      }
    }
  }

  switch (actions.performAction(creep)) {
    case "success":
    case "failure":
      delete creep.memory.action;
  }
}

export = { getBodyParts, run };
