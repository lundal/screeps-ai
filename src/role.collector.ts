import actions = require("./actions");
import bodies = require("./bodies");

function getBodyParts(energyLimit: number): BodyPartConstant[] | undefined {
  return bodies.build(
    [MOVE, CARRY, CARRY],
    [MOVE, CARRY, CARRY],
    energyLimit,
    15,
  );
}

function run(creep: Creep) {
  if (!creep.memory.action) {
    if (creep.store.getUsedCapacity(RESOURCE_ENERGY) == 0) {
      let structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
        filter: (structure) =>
          structure.structureType == STRUCTURE_CONTAINER &&
          structure.store.getFreeCapacity(RESOURCE_ENERGY) <= 1000,
        maxRooms: 1,
      });
      let resource = creep.pos.findClosestByPath(FIND_DROPPED_RESOURCES, {
        filter: (resource) =>
          resource.resourceType == RESOURCE_ENERGY && resource.amount >= 50,
        maxRooms: 1,
      });
      if (structure) {
        creep.memory.action = { type: "withdraw", targetId: structure.id };
      } else if (resource) {
        creep.memory.action = { type: "pickup", resourceId: resource.id };
      }
    } else {
      let structure = creep.pos.findClosestByPath(FIND_MY_STRUCTURES, {
        filter: (structure) =>
          structure.structureType == STRUCTURE_STORAGE &&
          structure.store.getFreeCapacity(RESOURCE_ENERGY) >=
            creep.store.getUsedCapacity(RESOURCE_ENERGY),
        maxRooms: 1,
      });
      if (structure) {
        creep.memory.action = { type: "transfer", targetId: structure.id };
      }
    }
  }

  switch (actions.performAction(creep)) {
    case "success":
    case "failure":
      delete creep.memory.action;
  }
}

export = { getBodyParts, run };
