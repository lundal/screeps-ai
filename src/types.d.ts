export interface RoleModule {
  getBodyParts(energyLimit: number): BodyPartConstant[] | undefined;
  run(creep: Creep);
}

export type Role =
  | "harvester"
  | "static-harvester"
  | "distributor"
  | "collector"
  | "upgrader"
  | "builder";

export type HarvestAction = {
  type: "harvest";
  sourceId: Id<Source>;
  containerId?: Id<StructureContainer>;
  forever: boolean;
};

export type PickUpAction = {
  type: "pickup";
  resourceId: Id<Resource>;
};

export type WithdrawAction = {
  type: "withdraw";
  targetId: Id<Structure | Tombstone | Ruin>;
};

export type TransferAction = {
  type: "transfer";
  targetId: Id<Structure | AnyCreep>;
};

export type BuildAction = {
  type: "build";
  targetId: Id<ConstructionSite>;
};

export type UpgradeAction = {
  type: "upgrade";
  controllerId: Id<StructureController>;
};

export type Action =
  | HarvestAction
  | PickUpAction
  | WithdrawAction
  | TransferAction
  | BuildAction
  | UpgradeAction;

export type ActionResult = "in-progress" | "success" | "failure";
