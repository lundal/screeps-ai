import actions = require("./actions");
import bodies = require("./bodies");

function getBodyParts(energyLimit: number): BodyPartConstant[] | undefined {
  return bodies.build(
    [MOVE, CARRY, WORK],
    [MOVE, CARRY, WORK],
    energyLimit,
    15,
  );
}

function run(creep: Creep) {
  if (!creep.memory.action) {
    if (creep.store.getUsedCapacity(RESOURCE_ENERGY) == 0) {
      let structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
        filter: (structure) =>
          (structure.structureType == STRUCTURE_STORAGE ||
            structure.structureType == STRUCTURE_CONTAINER) &&
          structure.store.getUsedCapacity(RESOURCE_ENERGY) >=
            creep.store.getFreeCapacity(RESOURCE_ENERGY),
        maxRooms: 1,
      });
      let resource = creep.pos.findClosestByPath(FIND_DROPPED_RESOURCES, {
        filter: (resource) =>
          resource.resourceType == RESOURCE_ENERGY &&
          resource.amount >= creep.store.getFreeCapacity(RESOURCE_ENERGY),
        maxRooms: 1,
      });
      if (structure) {
        creep.memory.action = { type: "withdraw", targetId: structure.id };
      } else if (resource) {
        creep.memory.action = { type: "pickup", resourceId: resource.id };
      }
    } else {
      let constructionSite = creep.pos.findClosestByPath(
        FIND_CONSTRUCTION_SITES,
        { maxRooms: 1 },
      );
      if (constructionSite) {
        creep.memory.action = { type: "build", targetId: constructionSite.id };
      } else if (creep.room.controller) {
        creep.memory.action = {
          type: "upgrade",
          controllerId: creep.room.controller.id,
        };
      }
    }
  }

  switch (actions.performAction(creep)) {
    case "success":
    case "failure":
      delete creep.memory.action;
  }
}

export = { getBodyParts, run };
