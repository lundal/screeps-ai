import actions = require("./actions");
import bodies = require("./bodies");

function getBodyParts(energyLimit: number): BodyPartConstant[] | undefined {
  return bodies.build(
    [MOVE, CARRY, CARRY],
    [MOVE, CARRY, CARRY],
    energyLimit,
    15,
  );
}

function run(creep: Creep) {
  if (!creep.memory.action) {
    if (creep.store.getUsedCapacity(RESOURCE_ENERGY) == 0) {
      let resource = creep.pos.findClosestByPath(FIND_DROPPED_RESOURCES, {
        filter: (resource) =>
          resource.resourceType == RESOURCE_ENERGY &&
          resource.amount >= creep.store.getFreeCapacity(RESOURCE_ENERGY),
        maxRooms: 1,
      });
      let structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
        filter: (structure) =>
          (structure.structureType == STRUCTURE_STORAGE ||
            structure.structureType == STRUCTURE_CONTAINER) &&
          structure.store.getUsedCapacity(RESOURCE_ENERGY) >=
            creep.store.getFreeCapacity(RESOURCE_ENERGY),
        maxRooms: 1,
      });
      if (resource) {
        creep.memory.action = { type: "pickup", resourceId: resource.id };
      } else if (structure) {
        creep.memory.action = { type: "withdraw", targetId: structure.id };
      }
    } else {
      let structures: (StructureTower | StructureExtension | StructureSpawn)[] =
        creep.room.find(FIND_STRUCTURES, {
          filter: (structure) =>
            (structure.structureType == STRUCTURE_TOWER ||
              structure.structureType == STRUCTURE_EXTENSION ||
              structure.structureType == STRUCTURE_SPAWN) &&
            structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0,
        });
      let prioritizedStructures = _.sortBy(
        structures,
        (structure) =>
          structure.store.getFreeCapacity(RESOURCE_ENERGY) /
          structure.store.getCapacity(RESOURCE_ENERGY) /
          -(1 + creep.pos.getRangeTo(structure)),
      );
      let structure = _.find(prioritizedStructures);
      if (structure) {
        creep.memory.action = { type: "transfer", targetId: structure.id };
      }
    }
  }

  switch (actions.performAction(creep)) {
    case "success":
    case "failure":
      delete creep.memory.action;
  }
}

export = { getBodyParts, run };
