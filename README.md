# Screeps AI

## Build

1. Install Node.js and npm
2. `npm run build`

## Deploy

1. Create a .env file with the following contents:
   ```dotenv
   SCREEPS_BRANCH=my-branch
   SCREEPS_TOKEN=my-api-token
   ```
2. `npm run deploy`
